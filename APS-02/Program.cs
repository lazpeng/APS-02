﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace APS_02
{

    /// Implements the AES-128 algo (locked to 128 for simplicity)
    class Cipher {
        static readonly byte[] Encryption_SBox = {
            0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
            0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
            0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
            0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
            0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
            0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
            0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
            0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
            0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
            0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
            0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
            0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
            0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
            0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
            0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
            0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
        };
        static readonly byte[] Decryption_SBox = {
            0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
            0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
            0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
            0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
            0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
            0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
            0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
            0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
            0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
            0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
            0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
            0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
            0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
            0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
            0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
            0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d
        };

        private const int BLOCK_SIZE = 16;
        private const int KEY_SIZE = 16;
        private const int BLOCK_ROW_SIZE = BLOCK_SIZE / 4;
        private const int NUM_ROUNDS = 10;
        private const int SBOX_1D_SIZE = 16;
        private byte[] original_key = new byte[KEY_SIZE];
        private readonly byte[] expanded_key = new byte[BLOCK_SIZE * (NUM_ROUNDS + 1)];
        private readonly byte[] state = new byte[BLOCK_SIZE];

        public Cipher(string password)
        {
            var hasher = System.Security.Cryptography.SHA256.Create();
            var hash = hasher.ComputeHash(System.Text.Encoding.Unicode.GetBytes(password));

            if(hash.Length < original_key.Length) {
                throw new Exception("Erro : Hash menor que o tamanho da key. Inválido");
            }

            for(int i = 0; i < original_key.Length; ++i) {
                original_key[i] = hash[i];
            }
        }

        private string EncodeBytes(byte[] b) {
            return System.Convert.ToBase64String(b, Base64FormattingOptions.None);
        }

        private byte SubstituteByte(byte b, byte[] SBox) {
            byte row, col;

            row = (byte)((b & 0xF0) >> 4);
            col = (byte)(b & 0x0F);

            int index = SBOX_1D_SIZE * row + col;

            return SBox[index];
        }

        private void SubBytes(byte[] SBox) {
            for(int i = 0; i < state.Length; ++i) {
                state[i] = SubstituteByte(state[i], SBox);
            }
        }

        private void ShiftRowsForward() {
            for(int i = 1; i < 4; ++i) {
                int row = i * 4;
                for(int j = 0; j < i; ++j) {
                    byte first = state[row];
                    state[row] = state[row + 1];
                    state[row + 1] = state[row + 2];
                    state[row + 2] = state[row + 3];
                    state[row + 3] = first;
                }
            }
        }

        private void ShiftRowsReverse() {
            for(int i = 1; i < 4; ++i) {
                int row = i * 4;
                for(int j = 0; j < i; ++j) {
                    byte last = state[row + 3];
                    state[row + 3] = state[row + 2];
                    state[row + 2] = state[row + 1];
                    state[row + 1] = state[row];
                    state[row] = last;
                }
            }
        }

        private void MixColumnsForward() {
            for(int i = 0; i < BLOCK_ROW_SIZE; ++i) {
                int row = i * BLOCK_ROW_SIZE;
                state[row + 0] = (byte)(FFMul(2, state[row]) ^ FFMul(3, state[row + 1]) ^ FFMul(1, state[row + 2]) ^ FFMul(1, state[row + 3]));
                state[row + 1] = (byte)(FFMul(1, state[row]) ^ FFMul(2, state[row + 1]) ^ FFMul(3, state[row + 2]) ^ FFMul(1, state[row + 3]));
                state[row + 2] = (byte)(FFMul(1, state[row]) ^ FFMul(1, state[row + 1]) ^ FFMul(2, state[row + 2]) ^ FFMul(3, state[row + 3]));
                state[row + 3] = (byte)(FFMul(3, state[row]) ^ FFMul(1, state[row + 1]) ^ FFMul(1, state[row + 2]) ^ FFMul(2, state[row + 3]));
            }
        }

        private void MixColumnsReverse() {
            for(int i = 0; i < BLOCK_ROW_SIZE; ++i) {
                int row = i * BLOCK_ROW_SIZE;
                state[row + 0] = (byte)(FFMul(0xe, state[row]) ^ FFMul(0xb, state[row + 1]) ^ FFMul(0xd, state[row + 2]) ^ FFMul(0x9, state[row + 3]));
                state[row + 1] = (byte)(FFMul(0x9, state[row]) ^ FFMul(0xe, state[row + 1]) ^ FFMul(0xb, state[row + 2]) ^ FFMul(0xd, state[row + 3]));
                state[row + 2] = (byte)(FFMul(0xd, state[row]) ^ FFMul(0x9, state[row + 1]) ^ FFMul(0xe, state[row + 2]) ^ FFMul(0xb, state[row + 3]));
                state[row + 3] = (byte)(FFMul(0xb, state[row]) ^ FFMul(0xd, state[row + 1]) ^ FFMul(0x9, state[row + 2]) ^ FFMul(0xe, state[row + 3]));
            }
        }

        private void AddRoundKey(int offset) {
            for(int i = 0; i < state.Length; ++i) {
                state[i] ^= expanded_key[offset + i];
            }
        }

        private void AddInputToState(byte[] input, int offset) {
            // Zero state
            for(int i = 0; i < state.Length; ++i) state[i] = 0;

            int len = input.Length - offset >= BLOCK_SIZE ? BLOCK_SIZE : input.Length - offset;

            // Copy input
            for(int i = 0; i < len; ++i) state[i] = input[offset + i];
        }

        /// Adapted from https://github.completionlist/DarkRyu550/aes.c/blob/master/aes.c
        private byte FFMul(byte a, byte b)
        {
            byte prod = 0;
            bool carry = false;

            for(int i = 0; i < 8; ++i) {
                if(a == 0 || b == 0) break;

                if((b & 1) != 0) prod ^= a;
                b >>= 1;

                carry = (a & 0x80) != 0;
                a <<= 1;

                if(carry) a ^= 0x1b;
            }

            return prod;
        }

        private int ReadIntFrom(byte[] src, int start) {
            if (start + 3 >= src.Length) {
                throw new Exception("ReadIntFrom : Out of bounds read");
            }

            int v = 0;

            v  = src[start + 3];
            v |= (int)src[start + 2] << 8;
            v |= (int)src[start + 1] << 16;
            v |= (int)src[start]     << 24;

            return v;
        }

        private void WriteIntTo(byte[] dest, int start, int val) {
            if(start + 3 >= dest.Length) {
                throw new Exception("WriteIntTo : Out of bounds write");
            }

            dest[start + 3] = (byte)(val & 0xFF);
            dest[start + 2] = (byte)((val & 0xFF00) >> 8);
            dest[start + 1] = (byte)((val & 0xFF0000) >> 16);
            dest[start]     = (byte)((val & 0xFF000000) >> 24);
        }

        private void ExpandKey()
        {
            // Copy the original key

            for(int i = 0; i < original_key.Length; ++i) {
                expanded_key[i] = original_key[i];
            }

            for(int i = original_key.Length; i < BLOCK_SIZE * NUM_ROUNDS;) {
                int prevint = ReadIntFrom(expanded_key, i - 4);
                if(i % original_key.Length == 0) {
                    // Rotate the previous block of 4 bytes
                    byte tmp = expanded_key[i - 4];
                    expanded_key[i - 4] = expanded_key[i - 3];
                    expanded_key[i - 3] = expanded_key[i - 2];
                    expanded_key[i - 2] = expanded_key[i - 1];
                    expanded_key[i - 1] = tmp;

                    // Substitute the previous' block 4 bytes
                    for(int j = 0; j < 4; ++j) {
                        expanded_key[(i - 4) + j] = SubstituteByte(expanded_key[(i - 4) + j], Encryption_SBox);
                    }

                    // Read the previous again 'cause we modified the expanded key
                    prevint = ReadIntFrom(expanded_key, i - 4);

                    // Do this... thing
                    prevint ^= 0x01000000 << (i / (original_key.Length / 4) - 1);
                }

                int klprevint = ReadIntFrom(expanded_key, i - original_key.Length);
                WriteIntTo(expanded_key, i, klprevint ^ prevint);

                i += 4;
            }
        }

        private void EncryptState()
        {
            int round_key_index = 0;

            AddRoundKey(round_key_index);
            round_key_index += BLOCK_SIZE;

            for(int i = 0; i < NUM_ROUNDS - 1; ++i) {
                SubBytes(Encryption_SBox);
                ShiftRowsForward();
                //MixColumnsForward();
                AddRoundKey(round_key_index);
                round_key_index += BLOCK_SIZE;
            }

            SubBytes(Encryption_SBox);
            ShiftRowsForward();
            AddRoundKey(round_key_index);
        }

        public string RawEncrypt(byte[] input) {
            ExpandKey();

            List<byte> output = new List<byte>();
            int offset = 0;

            while(offset < input.Length) {
                AddInputToState(input, offset);

                EncryptState();

                offset += BLOCK_SIZE;

                foreach(byte b in state) output.Add(b);
            }

            return EncodeBytes(output.ToArray());
        }

        /// Encrypts the given input into a base64 string
        public string Encrypt(string input) {
            byte[] input_bytes = Encoding.Unicode.GetBytes(input);

            return RawEncrypt(input_bytes);
        }

        /// Decodes the base64-encoded (and encrypted) input
        public string Decrypt(string encoded) {
            var bytes = System.Convert.FromBase64String(encoded);
            return RawDecrypt(bytes);
        }

        private void DecryptState()
        {
            int key_offset = BLOCK_SIZE * NUM_ROUNDS;

            AddRoundKey(key_offset);
            key_offset -= BLOCK_SIZE;

            for(int i = 0; i < NUM_ROUNDS - 1; ++i) {
                ShiftRowsReverse();
                SubBytes(Decryption_SBox);
                AddRoundKey(key_offset);
                key_offset -= BLOCK_SIZE;
                //MixColumnsReverse();
            }

            SubBytes(Decryption_SBox);
            ShiftRowsReverse();

            AddRoundKey(key_offset);
        }

        public string RawDecrypt(byte[] encrypted) {
            ExpandKey();

            int offset = 0;

            List<byte> output = new List<byte>();

            while(offset < encrypted.Length) {

                AddInputToState(encrypted, offset);

                DecryptState();

                offset += BLOCK_SIZE;

                foreach(byte b in state) output.Add(b);
            }

            return Encoding.Unicode.GetString(output.ToArray());
        }
    }

    class Program
    {
        static string GetKeyShadowTyped()
        {
            string password = "";

            while(true) {
                ConsoleKeyInfo info;

                info = Console.ReadKey(true);
                if(info.Key == ConsoleKey.Backspace) {
                    // erase last character
                    if(password.Length > 0)
                        password.Remove(password.Length - 1);
                } else if(info.Key == ConsoleKey.Enter) {
                    break;
                } else {
                    password += info.KeyChar;
                }
            }

            return password;
        }

        static string GetMessageFromFile(string filename)
        {
            StreamReader sr = new StreamReader(filename);
            return sr.ReadToEnd();
        }

        static void WriteToFile(string message, string filename)
        {
            StreamWriter sw = new StreamWriter(filename);
            sw.WriteLine(message);
            sw.Flush();
            sw.Close();
        }

        static void Main(string[] args)
        {
            if(args.Length > 0) {

            } else {
                while(true) {
                    Console.WriteLine("Escolha um modo de operação.");
                    Console.WriteLine("[0] - Sair");
                    Console.WriteLine("[1] - Encryptar uma mensagem");
                    Console.WriteLine("[2] - Decryptar uma mensagem");
                    Console.Write("> ");
                    var choice = Console.ReadLine();
                    bool encrypt;
                    if(choice == "0") {
                        break;
                    } else if(choice == "1") {
                        encrypt = true;
                    } else if(choice == "2") {
                        encrypt = false;
                    } else {
                        Console.WriteLine("A opção {0} não foi compreendida. Tente novamente", choice);
                        continue;
                    }

                    Console.WriteLine("Escolha um método de entrada para a mensagem:");
                    Console.WriteLine("[0] - Sair");
                    Console.WriteLine("[1] - Digitar a mensagem no console");
                    Console.WriteLine("[2] - Ler a mensagem de um arquivo");
                    Console.Write("> ");
                    var msg_in_type = Console.ReadLine();

                    string message;
                    if(msg_in_type == "0") {
                        break;
                    } else if(msg_in_type == "1") {
                        Console.Write("Mensagem >");
                        message = Console.ReadLine();
                    } else if(msg_in_type == "2") {
                        Console.WriteLine("Digite o nome do arquivo : ");
                        Console.Write("> ");
                        message = GetMessageFromFile(Console.ReadLine());
                    } else {
                        Console.WriteLine("A opção {0} não foi compreendida.", msg_in_type);
                        continue;
                    }

                    Console.Write("Agora digite a senha/chave : ");
                    string pass = GetKeyShadowTyped();

                    //try {
                        Cipher cipher = new Cipher(pass);
                        string result = "";
                        if(encrypt) {
                            result = cipher.Encrypt(message);
                        } else {
                            result = cipher.Decrypt(message);
                        }
                        Console.WriteLine("O resultado é \"{0}\".", result);
                        Console.WriteLine("Você deseja escrever o resultado pra um arquivo? Digite o nome do arquivo para sim ou só dê enter para não.");
                        Console.Write("Arquivo >");
                        string out_file = Console.ReadLine();
                        if(out_file.Length == 0) {
                            continue;
                        } else {
                            WriteToFile(result, out_file);
                        }
                    //} catch (Exception e) {
                    //  Console.WriteLine("Um erro ocorreu durante o procedimento: {0}", e.Message);
                    //}
                }
            }
        }
    }
}
