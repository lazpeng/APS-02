﻿using System;
using System.Text;
using APS02GUI;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
        notebook.CurrentPage = 0;
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnClick(object sender, EventArgs e)
    {
        string password = password_entry.Text;
        string text = text_entry.Buffer.Text;

        if(password.Length == 0 || text.Length == 0)
        {
            return;
        }

        Cipher c = new Cipher(password);

        try {

            byte[] result = c.RawEncrypt(Encoding.Unicode.GetBytes(text));

            ConversionFormat format;

            switch(output_encoding.ActiveText.ToLower())
            {
                case "base 64":
                    format = ConversionFormat.Base64;
                    break;
                case "hexadecimal":
                    format = ConversionFormat.Hex;
                    break;
                default:
                    throw new Exception("Erro fatal : Formato de saída inválido");
            }

            output_view.Buffer.Text = ConversionProvider.GetString(result, format);
        }
        catch (Exception exc)
        {
            MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, "{0}", exc.Message);
            dialog.Show();
        }
    }

    protected void Decrypt(object sender, EventArgs e)
    {
        string password = dec_password.Text;
        string text = dec_text.Buffer.Text;

        if (password.Length == 0 || text.Length == 0)
        {
            return;
        }

        Cipher c = new Cipher(password);

        ConversionFormat format;

        switch(text_format.ActiveText.ToLower())
        {
            case "base 64":
                format = ConversionFormat.Base64;
                break;
            case "hexadecimal":
                format = ConversionFormat.Hex;
                break;
            default:
                throw new Exception("Erro fatal : Formato de entrada inválido");
        }

        try {
            byte[] encoded_text = ConversionProvider.GetBytes(text, format);

            byte[] result = c.RawDecrypt(encoded_text);

            decrypt_output.Buffer.Text = Encoding.Unicode.GetString(result);
        }
        catch (Exception exc)
        {
            MessageDialog dialog = new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, "{0}", exc.Message);
            dialog.Show();
        }
    }
}
