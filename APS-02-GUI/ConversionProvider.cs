﻿using System;
using System.Collections.Generic;

namespace APS02GUI
{
    public enum ConversionFormat
    {
        Base64,
        Hex,
    }

    public static class ConversionProvider
    {
        public static string GetString(byte[] bytes, ConversionFormat fmt)
        {
            switch (fmt)
            {
                case ConversionFormat.Base64:
                    return Convert.ToBase64String(bytes);
                case ConversionFormat.Hex:
                    string f = "";

                    foreach(byte b in bytes)
                    {
                        string fs = String.Format("{0:X}", b);
                        if (fs.Length < 2) fs += '0';
                        f += fs;
                    }

                    return f;
                default:
                    return "";
            }
        }

        public static byte[] GetBytes(string text, ConversionFormat fmt)
        {
            switch(fmt)
            {
                case ConversionFormat.Base64:
                    return Convert.FromBase64String(text);
                case ConversionFormat.Hex:
                    if (text.Length % 2 != 0)
                    {
                        throw new Exception("Hex em um formato inválido (número impar de caracteres)");
                    }

                    byte[] result = new byte[text.Length / 2];
                    int index = 0;

                    for(int i = 0; i < text.Length;)
                    {
                        byte upper = Convert.ToByte(text.Substring(i, 1), 16);
                        byte lower = Convert.ToByte(text.Substring(i + 1, 1), 16);

                        result[index] = (byte)((upper << 4) | lower);

                        i += 2;
                        index += 1;
                    }

                    return result;
                default:
                    throw new Exception("Formato inválido");
            }
        }
    }
}
